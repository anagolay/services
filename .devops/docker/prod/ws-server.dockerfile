FROM docker.io/node:18.4.0-bullseye-slim as base

# this is the v6.30.0
ADD https://ipfs.anagolay.network/ipfs/QmeCUX9cK4YKdTbNVq3jg5cJPvz8uQiQmb4AKKd7niy4kY /usr/local/bin/pnpm

RUN chmod +x /usr/local/bin/pnpm 

# smoke test
RUN pnpm --version \
	&& node --version

WORKDIR /build

COPY . .

RUN node common/scripts/install-run-rush.js install \
	&& node common/scripts/install-run-rush.js update \
	&& node common/scripts/install-run-rush.js rebuild --verbose


# MAIN IMAGE FOR THE UPLOAD
FROM docker.io/node:18.4.0-bullseye-slim
LABEL maintainer="daniel@woss.io" 
LABEL description="Production ready Websocket microservice"
LABEL "network.anagolay.vendor"="Anagolay Network"

ENV PORT=3000


# this is the v6.30.0
COPY --from=base /usr/local/bin/pnpm /usr/local/bin/pnpm

RUN chmod +x /usr/local/bin/pnpm 

WORKDIR /app

COPY --from=base /build/services/ws-server/lib /app
COPY --from=base /build/services/ws-server/package.json /app
COPY --from=base /build/services/ws-server/LICENSE /app

RUN pnpm install --no-lockfile --silent --prod

EXPOSE ${PORT}

CMD [ "node","start.js" ]