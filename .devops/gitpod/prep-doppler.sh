#!/usr/bin/env bash
export HISTIGNORE='DOPPLER_*'

PROJECT_ROOT=$(git rev-parse --show-toplevel)

echo $DOPPLER_MACULA_TOKEN | doppler configure set token --silent --scope $PROJECT_ROOT/kelp/macula
echo $DOPPLER_IPFS_API_AUTH_TOKEN | doppler configure set token --silent --scope $PROJECT_ROOT/services/ipfs-api-auth-proxy
echo $DOPPLER_WEB_SOCKET | doppler configure set token --silent --scope $PROJECT_ROOT/services/ws-server
echo $DOPPLET_IPFS_CLI_TOKEN | doppler configure set token --silent --scope $PROJECT_ROOT/tools/ipfs-cli

source $HOME/.bashrc
