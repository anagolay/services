# Websocket service

The easiest way to start this service is using OCI image we are hosting here https://hub.docker.com/repository/docker/anagolay/microservices-ws-server. We suggest using the `latest` tags ALWAYS.

This service first and foremost is used to enable the 1-1 communication between the workflow builder and CLI.

> This service might not be used in the future but we need it now, do NOT relay on it too much.
