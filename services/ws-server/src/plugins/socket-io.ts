import { FastifyPluginAsync } from 'fastify';
import fp from 'fastify-plugin';
import { Server, ServerOptions } from 'socket.io';

/**
 * SocketIO fastify plugin. It changes the namespace for client to `ws` instead of `socket.io`
 * @param fastify - Fastify instance
 * @param options - Any Socket.IO options are supported
 * @public
 */
const socketIoPlugin: FastifyPluginAsync<Partial<ServerOptions>> = async (fastify, options) => {
  const io: Server = new Server(fastify.server, {
    cors: {
      // origin: "http://localhost:7766",
      origin: '*',
      allowedHeaders: '*'
    },
    path: '/ws/',
    ...options
  });

  fastify.decorate('io', io);

  fastify.addHook('onClose', async (fastify): Promise<void> => {
    console.info('Closing the SocketIO connection');

    fastify.io.close();
  });
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default fp(socketIoPlugin as any, '3.x') as any;
