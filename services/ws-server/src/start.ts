import Fastify, { FastifyReply, FastifyRequest } from 'fastify';
import { FastifyInstance } from 'fastify';

import setupRemoteSignerNamespace from './namespaces/remote-signer';
import setupWorkflowNamespace from './namespaces/workflow';
// import { join } from 'path';
import socketIoPlugin from './plugins/socket-io';

/**
 * Main entry point
 * @public
 */
export async function main(): Promise<void> {
  const fastify: FastifyInstance = Fastify({
    logger: true
  });

  // assign the socketIo Plugin
  fastify.register(socketIoPlugin);
  fastify.register(require('fastify-cors'), {
    origin: '*'
  });

  // fastify.register(require('fastify-static'), {
  //   root: join(__dirname, 'public')
  //   // prefix: '//' // optional: default '/'
  // });

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    // reply.header('Content-Type', 'text/html').send(await readFile(__dirname + '/index.html'));
    reply.send('Nothing to see here, move on!');
  });

  try {
    await fastify.listen(parseInt(process.env.AN_WS_PORT || '3000', 10), '0.0.0.0');

    const { io } = fastify;

    setupWorkflowNamespace(io);
    setupRemoteSignerNamespace(io);
    io.sockets.on('disconnect', function () {
      console.log('got disconnect');

      // handle disconnect
      io.sockets.disconnect();
      io.sockets.close();
    });
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }

  process.on('SIGTERM', () => {
    console.log('got SIGTERM');

    fastify.close(() => {
      console.log('Process terminated');
    });
  });
}

main().catch(console.error);
