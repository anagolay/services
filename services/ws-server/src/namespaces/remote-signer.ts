import { Namespace, Server, Socket } from 'socket.io';
import { DefaultEventsMap } from 'socket.io/dist/typed-events';

export default async function setup(
  io: Server
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Promise<Namespace<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap, any>> {
  const namespace: Namespace = io.of(
    /^\/remote_signer_[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}/
  );

  namespace.on('connection', (socket: Socket) => {
    const namespace: Namespace = socket.nsp;
    console.log(`Client ${socket.id} connected to namespace ${namespace.name}`);

    // You can emit messages directly on the `namespace` object...
    // namespace.emit('message', `Successfully connected on namespace: ${namespace.name}`);

    // ...BUT--BUT, BUT, BUT--you must listen for messages coming from the clients
    // on the socket (`socket`) object, NOT the namespace:
    socket.on('pleaseSignThis', (data) => {
      console.log('A message was received from a client: ', JSON.stringify(data, null, 2));
      // AND if you want to do a broadcast emit -- which sends the message to
      // all the connected clients *except* for the sender -- you MUST use the
      // socket object (`socket`), as the `namespace` does not understand the
      // `broadcast` method:
      socket.broadcast.emit('pleaseSignThis', data);
    });

    socket.on('signedMessage', (data) => {
      console.log(`A message for signedMessage`, JSON.stringify(data, null, 2));
      // AND if you want to do a broadcast emit -- which sends the message to
      // all the connected clients *except* for the sender -- you MUST use the
      // socket object (`socket`), as the `namespace` does not understand the
      // `broadcast` method:
      socket.broadcast.emit('signedMessage', data);
    });
    socket.on('cancelSigning', (message) => {
      socket.broadcast.emit('cancelSigning', message);
    });

    socket.onAny((eventName, ...args) => {
      console.log(eventName, args);
    });
  });
  return namespace;
}
