/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @rushstack/typedef-var */
import { Static, Type } from '@sinclair/typebox';
import { Job, JobAttributesData } from 'agenda';
import { FastifyInstance, FastifyPluginOptions } from 'fastify';
import { ObjectId } from 'mongodb';
import { isNil } from 'ramda';

import { getDB } from '../../db';
import { collection as operationCollection } from '../../db/operationCollection';
import { collection as workflowCollection } from '../../db/workflowCollection';
import { cachedWorker, jobsCollectionName } from '../../job-scheduler';
import { AddOperationToQueuePayload, AddWorkflowToQueuePayload } from '../../jobs/types';

/**
 * Incoming Request
 */
export const AddToQueueRequest = Type.Object({
  context: Type.String(),
  payload: Type.Union([AddOperationToQueuePayload, AddWorkflowToQueuePayload])
});

/**
 * If the Version is already built we return this,
 * which is the structure of the collection
 */
const VersionDocumentSchema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  title: 'Version document schema',
  type: 'object',
  properties: {
    _id: {
      type: 'string'
    },
    manifest: {
      type: 'string'
    },
    repository: {
      type: 'string'
    },
    jobId: {
      type: 'string'
    },
    revision: {
      type: 'string'
    },
    artifacts: {
      type: 'object',
      properties: {
        performance: {
          type: 'object',
          properties: {
            execInSec: {
              type: 'number'
            }
          },
          required: ['execInSec']
        },
        items: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              artifactType: {
                type: 'object',
                additionalProperties: true
              },
              fileExtension: {
                type: 'string'
              },
              ipfsCid: {
                type: 'string'
              }
            },
            required: ['artifactType', 'fileExtension', 'ipfsCid']
          }
        }
      },
      required: ['performance', 'items']
    },
    buildOutput: {
      type: 'object',
      properties: {
        performance: {
          type: 'object',
          properties: {
            execInSec: {
              type: 'number'
            }
          },
          required: ['execInSec']
        },
        outputResult: {
          type: 'string'
        }
      },
      required: ['performance', 'outputResult']
    }
  },
  required: ['_id', 'jobId', 'artifacts', 'buildOutput', 'manifest']
};

/**
 * Response structure
 */
const JobAddedResponse = Type.Object({
  message: Type.String(),
  job: Type.Object({
    id: Type.String(),
    api: Type.String()
  })
});

/**
 * Add to Queue Route
 * @param fastify -
 * @param options -
 * @param done -
 */
export function createAddToQueue(
  fastify: FastifyInstance,
  options: FastifyPluginOptions,
  done: (err?: Error) => void
): void {
  // build options
  const opts: FastifyPluginOptions = {
    ...options,
    schema: {
      response: {
        201: JobAddedResponse,
        200: VersionDocumentSchema
      },
      body: AddToQueueRequest
    }
  };

  fastify.post<{ Body: Static<typeof AddToQueueRequest> }>('/', opts, async (req, reply) => {
    const context = req.body.context;

    function removeFailed(job: Job<JobAttributesData> | undefined): boolean {
      if (isNil(job)) {
        return false;
      }
      const isFailed = !isNil(job.attrs.failReason);
      if (isFailed) {
        job.remove();
      }
      return isFailed;
    }

    switch (context) {
      case 'operation': {
        const payload = req.body.payload as Static<typeof AddOperationToQueuePayload>;
        const mongoDocument = (await cachedWorker.jobs({ 'data.index': payload.revision })).shift();
        const isFailed = removeFailed(mongoDocument);

        if (isNil(mongoDocument) || isFailed) {
          const job = await cachedWorker
            .create('publishOperation', { index: payload.revision, context, payload })
            .unique({ 'data.index': payload.revision }, { insertOnly: true }) //https://www.npmjs.com/package/agenda#user-content-uniqueproperties-options
            .save();
          reply.code(201).send({
            message: 'Job created',
            job: {
              id: job.attrs._id,
              api: `/v1/q/${job.attrs._id}`
            }
          });
        } else {
          const collection = await operationCollection();
          const operation = await collection.findOne({ index: mongoDocument.attrs.data?.index });
          reply.send(operation);
        }
        break;
      }
      case 'workflow': {
        const payload = req.body.payload as Static<typeof AddWorkflowToQueuePayload>;
        const operationVersionIds = payload.manifestData.segments
          .flatMap((segment) => segment.sequence.flatMap((sequence) => sequence.versionId))
          .join(',');
        const mongoDocument = (await cachedWorker.jobs({ 'data.index': operationVersionIds })).shift();
        const isFailed = removeFailed(mongoDocument);

        if (isNil(mongoDocument) || isFailed) {
          const job = await cachedWorker
            .create('publishWorkflow', { index: operationVersionIds, context, payload })
            .unique({ 'data.index': operationVersionIds }, { insertOnly: true })
            .save();

          reply.code(201).send({
            message: 'Job created',
            job: {
              id: job.attrs._id,
              api: `/v1/q/${job.attrs._id}`
            }
          });
        } else {
          const collection = await workflowCollection();
          const workflow = await collection.findOne({ index: mongoDocument.attrs.data?.index });
          reply.send(workflow);
        }
        break;
      }
      default:
        throw new Error(`Context ${context} is not supported.`);
    }
  });
  done();
}

/**
 * Check the job ID for the success or error
 * @param fastify -
 * @param options -
 * @param done -
 */
export function checkTheJobId(
  fastify: FastifyInstance,
  options: FastifyPluginOptions,
  done: (err?: Error) => void
): void {
  // build options
  const opts: FastifyPluginOptions = {
    ...options
  };

  fastify.get<{ Params: { jobId: string } }>('/:jobId', opts, async (req, reply) => {
    try {
      const { jobId } = req.params;
      const db = await getDB();
      const doc: any = await db.collection(jobsCollectionName).findOne({ _id: new ObjectId(jobId) });

      if (isNil(doc)) {
        reply.code(404).send({ error: true, message: 'Job not found' });
      } else if (!isNil(doc.failReason)) {
        reply.code(400).send({ message: doc.failReason });
      } else if (isNil(doc.lastFinishedAt)) {
        reply.send({ done: false, message: 'Job is still running' });
      } else {
        let mongoDocument = null;
        switch (doc.data.context) {
          case 'operation':
            mongoDocument = await (await operationCollection()).findOne({ jobId });
            break;
          case 'workflow':
            mongoDocument = await (await workflowCollection()).findOne({ jobId });
            break;
          default:
        }
        if (mongoDocument) {
          delete (mongoDocument as any).index;
        }
        reply.send({ done: true, version: mongoDocument });
      }
    } catch (error: any) {
      console.error(error);
      reply.code(404).send({ error: true, message: error.message });
    }
  });
  done();
}
/**
 * Delete the job
 * @param fastify -
 * @param options -
 * @param done -
 */
export function deleteTheJob(
  fastify: FastifyInstance,
  options: FastifyPluginOptions,
  done: (err?: Error) => void
): void {
  // build options
  const opts: FastifyPluginOptions = {
    ...options
  };

  fastify.delete<{ Params: { jobId: string } }>('/:jobId', opts, async (req, reply) => {
    const { jobId } = req.params;
    const db = await getDB();
    const rmJob = await db.collection(jobsCollectionName).deleteOne({ _id: new ObjectId(jobId) });
    reply.send(rmJob);
  });
  done();
}
/**
 * List all jobs, there is no filter so you will get ALL. The list is sorted by the `lastFinishedAt` desc
 * @param fastify -
 * @param opts -
 * @param done -
 */
export function listAllJobs(
  fastify: FastifyInstance,
  options: FastifyPluginOptions,
  done: (err?: Error) => void
): void {
  // build options
  const opts: FastifyPluginOptions = {
    ...options
  };

  fastify.get('/', opts, async (req, reply) => {
    const db = await getDB();
    const jobs = await db.collection(jobsCollectionName).find().sort('-lastFinishedAt').toArray();
    jobs.forEach((job) => delete job.data.index);
    reply.send(jobs);
  });
  done();
}
