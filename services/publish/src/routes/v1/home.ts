import { FastifyInstance, FastifyPluginOptions, FastifyReply, FastifyRequest } from 'fastify';

/**
 * Home Route for the v1
 * @param fastify -
 * @param opts -
 * @param done -
 */
export default function (
  fastify: FastifyInstance,
  opts: FastifyPluginOptions,
  done: (err?: Error) => void
): void {
  fastify.get('/', (req: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'home route, nothing to see here' });
  });
  done();
}
