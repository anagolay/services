/* eslint-disable @rushstack/typedef-var */
import { Static, Type } from '@sinclair/typebox';

import { IResultWithPerformance } from '../utils/utils';

export type IWasmArtifactType = 'Esm' | 'Web' | 'Wasm' | 'Cjs';

export type IArtifactType = { Wasm: IWasmArtifactType } | { Docs: undefined } | { Git: undefined };

export interface IVersionArtifact {
  artifactType: IArtifactType;
  fileExtension: string;
  ipfsCid: string;
}

export interface IArtifacts extends IResultWithPerformance {
  items: IVersionArtifact[];
}

export interface IArtifactPayload {
  localRepoPath: string;
  extraPackageJson?: {
    repository?: {
      revision: string;
    };
    anagolay: {
      type: 'operation' | 'workflow';
      manifest: string;
    };
  };
}

export type AgendaJobPayload =
  | Static<typeof AddOperationToQueuePayload>
  | Static<typeof AddWorkflowToQueuePayload>;

export interface IAgendaJob {
  index: string;
  payload: AgendaJobPayload;
}

/**
 * Add Operation to queue payload
 */
export const AddOperationToQueuePayload = Type.Object({
  repository: Type.String(),
  revision: Type.String()
});

/**
 * Operation
 */

export const Operation = Type.Object({
  id: Type.String(),
  data: Type.Object({
    name: Type.String(),
    description: Type.String(),
    inputs: Type.Array(Type.String()),
    config: Type.Object({}),
    groups: Type.Array(Type.String()),
    output: Type.String(),
    repository: Type.String(),
    license: Type.String(),
    features: Type.Array(Type.String())
  }),
  extra: Type.Union([Type.Object({}), Type.Null()])
});

/**
 * OperationVersion
 */

export const OperationVersion = Type.Object({
  id: Type.String(),
  data: Type.Object({
    entityId: Type.String(),
    parentId: Type.Union([Type.String(), Type.Null()]),
    artifacts: Type.Array(
      Type.Object({
        artifactType: Type.Union([Type.String(), Type.Object({}, { additionalProperties: true })]),
        fileExtension: Type.String(),
        ipfsCid: Type.String()
      })
    )
  }),
  extra: Type.Object({
    createdAt: Type.String()
  })
});

/**
 * Add Workflow to queue payload
 */

export const OperationVersionReference = Type.Object({
  versionId: Type.String(),
  config: Type.Object({}, { additionalProperties: true }),
  inputs: Type.Optional(Type.Array(Type.String())),
  output: Type.Optional(Type.String())
});

export const Segment = Type.Object({
  inputs: Type.Array(Type.Integer()),
  sequence: Type.Array(OperationVersionReference)
});

export const AddWorkflowToQueuePayload = Type.Object({
  manifestData: Type.Object({
    name: Type.String(),
    description: Type.String(),
    groups: Type.Array(Type.String()),
    creators: Type.Array(Type.String()),
    segments: Type.Array(Segment)
  }),
  operations: Type.Array(Operation),
  operationVersions: Type.Array(OperationVersion)
});
