/* eslint-disable @typescript-eslint/no-explicit-any */
import { Static } from '@sinclair/typebox';
import { Agenda, Job } from 'agenda';
import { rm } from 'fs/promises';
import { performance } from 'perf_hooks';

import { insertOne, IOperationVersionDocument } from '../db/operationCollection';
import {
  createDocsWithIpfsUpload,
  createWasmArtifactsWithIpfsUpload,
  rehostGitRepo,
  wasmArtifact
} from '../utils/artifacts';
import exec, { executeMakers } from '../utils/exec';
import { cloneRepo, isDirty } from '../utils/git';
import createLogger, { Logger } from '../utils/logger';
import { ISpawnProcessReturn } from '../utils/spawn';
import { AddOperationToQueuePayload, IAgendaJob, IVersionArtifact } from './types';

// how we call this task
const taskName: string = 'publishOperation';

const log: Logger = createLogger({ name: taskName });

/**
 * Configure the Job
 * @param agenda -
 */
export default async function configure(agenda: Agenda): Promise<void> {
  agenda.define(taskName, { priority: 20, concurrency: 1 }, async (job: Job): Promise<any> => {
    log.info(`Started Operation publish task ${job.attrs._id}`);
    // Prevent this job from running again
    job.disable().save();

    const { index, payload } = job.attrs.data as IAgendaJob;
    const { repository, revision } = payload as Static<typeof AddOperationToQueuePayload>;
    try {
      const { repoPath: localRepoPath } = await cloneRepo(
        {
          subfolder: 'operation_publish',
          repository,
          revision
        },
        log
      );
      log.info('Format the code to see will we get the dirty repository ...');

      await exec('makers --makefile Makefile.toml format', { cwd: localRepoPath, shell: '/bin/bash' });
      const { dirty, changes: dirtyChanges } = await isDirty(localRepoPath);
      if (dirty) {
        throw new Error(
          JSON.stringify({
            reason: 'Git repository after the checks became dirty, cannot continue.',
            data: dirtyChanges
          })
        );
      }

      let artifacts: IVersionArtifact[] = [];
      const artifactsPerfStart = performance.now();

      // rehost the repository from remote repo
      const rehosted = await rehostGitRepo({
        repository,
        revision,
        unpack: true
      });
      artifacts.push(rehosted);

      // generate the manifest
      const manifest = await generateManifest(localRepoPath, log);
      // let's build the operation
      const buildOutput = await buildOperation(localRepoPath, log);

      // this will only work for ones that need compressing, wem and web
      const wasmArtifacts = await createWasmArtifactsWithIpfsUpload(
        {
          localRepoPath,
          extraPackageJson: {
            repository: {
              revision: revision
            },
            anagolay: {
              type: 'operation',
              manifest
            }
          }
        },
        log
      );
      artifacts = artifacts.concat(wasmArtifacts);

      // add the WASM artifact
      const wasm = await wasmArtifact(
        {
          localRepoPath
        },
        log
      );
      artifacts.push(wasm);

      // Upload docs
      const docs = await createDocsWithIpfsUpload(localRepoPath, log);
      artifacts.push(docs);

      const artifactsPerfEnd = (performance.now() - artifactsPerfStart) / 1000;

      // cleanup the mess
      await cleanup([localRepoPath], log);

      const operationVersionItem: IOperationVersionDocument = {
        index,
        manifest: '0x' + Buffer.from(manifest).toString('hex'),
        repository: repository,
        jobId: job.attrs._id?.toString() as string,
        revision: revision,
        artifacts: {
          performance: {
            execInSec: artifactsPerfEnd
          },
          items: artifacts
        },
        buildOutput
      };
      await insertOne(operationVersionItem);

      log.info('end of the task');
    } catch (error) {
      console.error('caught the error in the job', error);
      job.fail(error as string);
    }
  });
}

/**
 * Cleanup the env and containers
 * @param params -
 * @param log -
 */
async function cleanup(paths: string[], log: Logger): Promise<void> {
  log.info(`Cleaning up the paths`);
  paths.map(async (p) => {
    await rm(p, { recursive: true, force: true });
  });
}

/**
 * Generate the Operation manifest. The returned manifest will be as it is returned from the cli
 * It uses the
 * ```
 * cargo run --release -- --manifest
 * ```
 * to create the manifest and it ill fail if it cannot find that command
 *
 * @param repositoryLocation -
 * @param log - Pino Logger
 */
async function generateManifest(repositoryLocation: string, log: Logger): Promise<string> {
  log.info('Generating the manifest ...');
  const { stdout } = await exec(['cargo', 'run', '--release', '--', '--manifest'].join(' '), {
    cwd: repositoryLocation
  });

  return stdout;
}
/**
 * Build the operation. It uses  the `cargo make` or `makers` to build the artifacts and format the code
 *
 * Argument used for building is `build-all`
 *
 * @param repositoryLocation -
 * @param log - Pino Logger
 */
async function buildOperation(repositoryLocation: string, log: Logger): Promise<ISpawnProcessReturn> {
  log.info('Building the operation ...');
  return await executeMakers(['--makefile', 'Makefile.toml', 'operation-flow'], repositoryLocation, log);
}
