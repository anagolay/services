import { Db, MongoClient } from 'mongodb';
import { Logger } from 'pino';

import { envs } from '../utils/env';
import createLogger from '../utils/logger';
import { collectionName as operationCollection } from './operationCollection';
import { collectionName as workflowCollection } from './workflowCollection';

export const dbName: string = 'anagolay-publish';
export let mongoClient: MongoClient;
export let dbConnection: Db;

const log: Logger = createLogger({ name: 'mongoDB', level: 'info' });

/**
 * Setup the MongoDB collections and all the indexes. Set the cache for the DB connection and connected client
 * @returns MongoClient
 */
export async function setupMongoDB(): Promise<MongoClient> {
  if (mongoClient) {
    log.trace('DB client found, returning ...');
    return mongoClient;
  }

  mongoClient = new MongoClient(envs().MONGODB_CONNSTRING_WITH_DB);
  await mongoClient.connect();

  dbConnection = mongoClient.db(dbName);
  dbConnection.collection(operationCollection).createIndex('index', {
    unique: true
  });
  dbConnection.collection(workflowCollection).createIndex('index', {
    unique: true
  });

  return mongoClient;
}

/**
 * Return the MongoClient instance
 * @returns
 */
export async function getMongoClient(): Promise<MongoClient> {
  if (mongoClient) {
    log.trace('DB client found, returning ...');
    return mongoClient;
  }
  mongoClient = new MongoClient(envs().MONGODB_CONNSTRING_WITH_DB);
  return await mongoClient.connect();
}

/**
 * Return the DB class instance
 * @returns
 */
export async function getDB(): Promise<Db> {
  log.trace('Getting the DB connection ...');

  if (mongoClient && dbConnection) {
    return dbConnection;
  }

  dbConnection = mongoClient.db(dbName);

  return dbConnection;
}
