import { Collection, Document } from 'mongodb';

import { IArtifacts } from '../jobs/types';
import { ISpawnProcessReturn } from '../utils/spawn';
import { getDB } from '.';

export const collectionName: string = 'wv_artifacts';

export interface IWorkflowVersionDocument {
  jobId: string;
  index: string;
  manifest: string;
  artifacts: IArtifacts;
  buildOutput: ISpawnProcessReturn;
}

/**
 * Create the version document and return it
 * @param data -
 * @returns
 */
export async function insertOne(data: IWorkflowVersionDocument): Promise<Document> {
  const db = await getDB();
  return await db.collection(collectionName).insertOne(data);
}
/**
 * Return version document collection instance
 * @returns
 */
export async function collection(): Promise<Collection<IWorkflowVersionDocument>> {
  const db = await getDB();
  return db.collection(collectionName);
}
