import { cp, mkdir, readdir, readFile, writeFile } from 'fs/promises';
import { endsWith, find, isNil, mergeDeepLeft } from 'ramda';
import simpleGit from 'simple-git';
import tar from 'tar';

import { IArtifactPayload, IVersionArtifact, IWasmArtifactType } from '../jobs/types';
import copyright from '../templates/copyright';
import prettierRc from '../templates/prettierrc';
import exec, { executeMakers } from './exec';
import { gitCloneBare, IGitCloneBareOptions } from './git';
import { uploadViaAdd, uploadViaAddAll } from './ipfs';
import { Logger } from './logger';
import { ISpawnProcessReturn } from './spawn';

/**
 * Create the artifacts for these tech
 */

const createFor: IWasmArtifactType[] = ['Esm', 'Web', 'Cjs'];

/**
 * Rehost the repo to the IPFS and then remove it from the local FS
 * @param cloneOptions -
 */
export async function rehostGitRepo(cloneOptions: IGitCloneBareOptions): Promise<IVersionArtifact> {
  const bareRepo = await gitCloneBare(cloneOptions);

  const resFromIpfs = await uploadViaAddAll(bareRepo);

  await exec(`rm -rf ${bareRepo}`);

  return {
    artifactType: { Git: undefined },
    fileExtension: 'git',
    ipfsCid: resFromIpfs.cid
  };
}

/**
 * Host the folder as a git repo to the IPFS
 * @param localRepoPath - path of the files to host as bare repo
 */
export async function hostGitRepo(localRepoPath: string): Promise<IVersionArtifact> {
  // Initialize folder as git bare repo
  const git = simpleGit({ baseDir: localRepoPath });
  await git.init();

  await git.add('*');
  await git.addConfig('commit.gpgsign', 'false');
  await git.addConfig('user.name', 'Anagolay Publish Service');
  await git.addConfig('user.email', 'admin@anagolay.network');

  await git.commit('initial commit');

  const execOptions = {
    cwd: localRepoPath,
    shell: '/bin/bash'
  };

  const revision = (await exec('git rev-parse HEAD', execOptions)).stdout.trim();

  const bareRepo = await gitCloneBare({
    repository: localRepoPath,
    revision,
    unpack: false
  });

  const resFromIpfs = await uploadViaAddAll(bareRepo);

  return {
    artifactType: { Git: undefined },
    fileExtension: 'git',
    ipfsCid: resFromIpfs.cid
  };
}

/**
 * Create the tar artifacts and upload them to the IPFS node. This function changes the `package.json` to contain the Anagolay specific fields.
 * @param params  -
 * @param log -
 * @returns
 */
export async function createWasmArtifactsWithIpfsUpload(
  params: IArtifactPayload,
  log: Logger
): Promise<IVersionArtifact[]> {
  const { localRepoPath, extraPackageJson } = params;
  const artifactPath = `${localRepoPath}/artifacts`;

  const paths = await Promise.all(
    createFor.map(async (m) => {
      await mkdir(`${artifactPath}/${m.toLowerCase()}`, { recursive: true });
      return { path: `${localRepoPath}/pkg/release/${m.toLowerCase()}`, artifactType: m };
    })
  );

  log.info(`Creating the artifacts for: ${createFor.join(' and ')}`);
  const tarFiles = await Promise.all(
    paths.map(async ({ path, artifactType }) => {
      await cp(`${localRepoPath}/LICENSE`, `${path}/LICENSE`, { force: true });
      await writeFile(`${path}/.prettierrc`, prettierRc.toString(), { flag: 'w' });

      const packageJson = JSON.parse((await readFile(`${path}/package.json`)).toString());

      // update package.json with extra stuff
      let newPackageJson = JSON.stringify(packageJson, null, 2);
      if (!isNil(extraPackageJson)) {
        newPackageJson = JSON.stringify(mergeDeepLeft(packageJson, extraPackageJson), null, 2);
      }

      // replace the package.json with improved version
      await writeFile(`${path}/package.json`, newPackageJson, { flag: 'w' });

      const files = await readdir(path);

      // ADD copyright
      files.map(async (f) => {
        if (endsWith('.js', f) || endsWith('.d.ts', f)) {
          const realPath = [path, f].join('/');
          const fileContent = [copyright, (await readFile(realPath)).toString()].join('');
          await writeFile(realPath, fileContent, { flag: 'w' });
        }
      });

      // format the js code
      await formatJavascript(path, artifactType, log);

      const fileName = `${packageJson.name}.tar.gz`;
      const tarFilePath = `${artifactPath}/${artifactType.toLowerCase()}/${fileName}`;
      log.info(`Compressing the ${artifactType} artifact ...`);
      await tar.create(
        {
          gzip: true,
          file: tarFilePath,
          cwd: path,
          portable: true,
          filter: (path, stat) => {
            stat.mtime = null as unknown as number;
            return true;
          }
        },
        files
      );

      log.info(`Uploading the ${artifactType} artifact ...`);
      const ipfs = await uploadViaAdd(fileName, tarFilePath);

      return {
        artifactType: { Wasm: artifactType },
        fileExtension: 'tar.gz',
        ipfsCid: ipfs.cid
      };
    })
  );
  return tarFiles;
}

/**
 * Format the artifact code using the prettier and our .prettierrc. Invokes the `makers artifact-format`
 * @param path -
 * @param artifactType -
 * @param log -
 * @returns
 */
async function formatJavascript(
  path: string,
  artifactType: string,
  log: Logger
): Promise<ISpawnProcessReturn> {
  log.info(`Formatting the ${artifactType} artifact ...`);
  return await executeMakers(['--makefile', '../../../Makefile.toml', 'artifact-format'], path, log);
}

/**
 * Call the makers target to generate docs and upload them to the IPFS node. Returns the docs root on IPFS
 * @param localRepoPath -
 * @param log -
 * @public
 */
export async function createDocsWithIpfsUpload(
  localRepoPath: string,
  log: Logger
): Promise<IVersionArtifact> {
  log.info(`Generating docs ...`);
  await executeMakers(['--makefile', 'Makefile.toml', 'docs-flow'], localRepoPath, log);
  log.info(`Uploading docs ...`);
  const ipfs = await uploadViaAddAll(`${localRepoPath}/docs/api`);
  return {
    artifactType: { Docs: undefined },
    fileExtension: 'html',
    ipfsCid: ipfs.cid
  };
}

/**
 * Create only the WASM file, upload it to the ipfs and create artefact.
 *
 * Currently the WASM file is taken from the ESM build.
 * @param params -
 * @param log -
 * @returns
 */
export async function wasmArtifact(params: IArtifactPayload, log: Logger): Promise<IVersionArtifact> {
  const { localRepoPath } = params;
  // WASM artifact doesn't  require build, so we do it just after all the builds. Since ALL the wasm files are the same, we are going to take one from the ESM build

  const pkgPath = `${localRepoPath}/pkg/release/esm`;
  const packageJson = JSON.parse((await readFile(`${pkgPath}/package.json`)).toString());

  const files = await readdir(pkgPath);
  const wasmPath = [pkgPath, find(endsWith('.wasm'))(files)].join('/');

  const fileName = `${packageJson.name}.wasm`;

  log.info(`Uploading the WASM artifact ...`);
  const ipfs = await uploadViaAdd(fileName, wasmPath);

  return {
    artifactType: { Wasm: 'Wasm' },
    fileExtension: 'wasm',
    ipfsCid: ipfs.cid
  };
}
