import { readdir } from 'fs/promises';
import { resolve } from 'path';

export interface IResultWithPerformance {
  performance: {
    execInSec: number;
  };
}

/**
 * checks against the common value options for the true boolean value
 * @param value - Can be `'true' | '1' | 1`
 */
export function isTrue(value: string | number): boolean {
  return value === 'true' || value === '1' || value === 1 ? true : false;
}
/**
 * checks against the common value options for the false boolean value
 * @param value - Can be `'false' | '0' | 0`
 */
export function isFalse(value: string | number): boolean {
  return value === 'false' || value === '0' || value === 0 ? false : true;
}

/**
 * Read all files in a directory and its subdirectories, producing the
 * encountered file paths in output.
 * The difference between this and `fs/readdir` is: this function will also read the
 * files in the subdirectories.
 *
 * @param dir - The directory to read
 * @returns Generator of strings, one for each file path encountered in the
 * directory to read and its subdirectories
 */
export async function* readdirRecursive(dir: string): AsyncGenerator<string> {
  const entries = await readdir(dir, { withFileTypes: true });
  for (const entry of entries) {
    const res = resolve(dir, entry.name);
    if (entry.isDirectory()) {
      yield* readdirRecursive(res);
    } else {
      yield res;
    }
  }
}
