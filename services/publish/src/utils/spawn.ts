import { spawn, SpawnOptionsWithoutStdio } from 'child_process';
import { performance } from 'perf_hooks';

import { Logger } from './logger';
import { IResultWithPerformance } from './utils';

export interface ISpawnProcessReturn extends IResultWithPerformance {
  outputResult: string;
}

// docs https://nodejs.org/api/child_process.html#child_processspawncommand-args-options

/**
 * Async Spawn from child_process. collects all the stdout and returns when success. Also when logger passed logs in async way
 * @param command -
 * @param execParams -
 * @param opts -
 * @param log -
 * @returns
 */
export async function asyncSpawn(
  command: string,
  execParams: string[],
  opts: SpawnOptionsWithoutStdio,
  pipeToMainProcess: boolean = false,
  log?: Logger
): Promise<ISpawnProcessReturn> {
  return new Promise((resolve, reject) => {
    let outputResult: string = '';

    log?.info(`Executing: ${command} ${execParams.join(' ')}`);

    const start = performance.now();
    const spawnProcess = spawn(command, execParams, { ...opts });
    if (pipeToMainProcess) {
      spawnProcess.stdout.pipe(process.stdout);
      spawnProcess.stderr.pipe(process.stderr);
    } else {
      spawnProcess.stdout.on('data', (data) => {
        const dataString = data.toString();
        outputResult = outputResult.concat(dataString);
        log?.info(`${dataString}`);
      });

      spawnProcess.stderr.on('data', (data) => {
        log?.error(`stderr: ${data}`);
      });
    }

    // reject the promise
    spawnProcess.on('error', reject);

    // when the process is closed
    spawnProcess.on('close', (code) => {
      if (code !== 0) {
        log?.error(`Process exited with code ${code}`);
        reject(`Process exited with code ${code}`);
      } else {
        log?.info('Process successful 🎉');
        resolve({
          performance: {
            execInSec: (performance.now() - start) / 1000
          },
          outputResult: '0x' + Buffer.from(outputResult).toString('hex')
        });
      }
    });
  });
}

export default asyncSpawn;
