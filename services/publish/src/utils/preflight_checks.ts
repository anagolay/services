import { compose, map, prop, trim } from 'ramda';

import { verifyEnv } from './env';
import exec from './exec';

interface IRuntimePrograms {
  rust: string;
  cargo: string;
  wasmPack: string;
  git: string;
  nodejs: string;
}

export default async function runChecks(): Promise<IRuntimePrograms> {
  const resFromPromises = {
    tar: await exec(`tar --version`),
    rust: await exec(`rustc --version`),
    cargo: await exec(`cargo --version`),
    cargoMake: await exec(`makers --version`),
    wasmPack: await exec(`wasm-pack --version`),
    git: await exec(`git --version`),
    nodejs: await exec(`node --version`)
  };

  // go through the list get the stdout and trim the output
  const res = compose(map(trim), map(prop('stdout')))(resFromPromises) as unknown as IRuntimePrograms;

  verifyEnv([
    'APP_PORT',
    'IPFS_API_URL',
    'IPFS_API_KEY',
    'IPFS_PIN',
    'IPFS_GATEWAY_HOSTNAME',
    'MONGODB_USERNAME',
    'MONGODB_PASSWORD',
    'MONGODB_PORT'
  ]);

  return res;
}
