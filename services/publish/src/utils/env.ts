import { forEach, isNil } from 'ramda';

import { dbName } from '../db';
import { isTrue } from './utils';

// eslint-disable-next-line @rushstack/typedef-var

/**
 * Verify that the environment variables are loaded and not empty.
 *
 * Pass the [] if you don't want verification
 * @param keys -
 */
export function verifyEnv(keys: string[]): void {
  forEach((k: string) => {
    if (isNil(process.env[k])) {
      throw new Error(`Missing env variables: ${k}`);
    }
  }, keys);
  // check do we have the needed env, it will throw error on null and undefined
}

/**
 * Return parsed envs
 * @returns
 */
export function envs(): {
  APP_PORT: number;
  IPFS_API_URL: string;
  IPFS_API_KEY: string;
  IPFS_PIN: boolean;
  IPFS_GATEWAY_HOSTNAME: string;
  MONGODB_HOSTNAME: string;
  MONGODB_USERNAME: string;
  MONGODB_PASSWORD: string;
  MONGODB_CONNSTRING: string;
  MONGODB_CONNSTRING_WITH_DB: string;
  APPROVED_API_KEY: string;
  ENABLE_API_KEY_SUPPORT: boolean;
} {
  const {
    APP_PORT = 3000,
    IPFS_API_URL,
    IPFS_PIN,
    IPFS_GATEWAY_HOSTNAME,
    MONGODB_HOSTNAME = 'localhost',
    MONGODB_PORT = 27017,
    MONGODB_USERNAME,
    MONGODB_PASSWORD,
    APPROVED_API_KEY,
    ENABLE_API_KEY_SUPPORT = false,
    IPFS_API_KEY
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } = process.env as any;

  return {
    APP_PORT: parseInt(APP_PORT, 10),
    IPFS_API_URL,
    IPFS_API_KEY,
    IPFS_PIN: isTrue(IPFS_PIN),
    IPFS_GATEWAY_HOSTNAME,
    APPROVED_API_KEY,
    ENABLE_API_KEY_SUPPORT,
    MONGODB_HOSTNAME,
    MONGODB_USERNAME,
    MONGODB_PASSWORD,
    MONGODB_CONNSTRING: `mongodb://${MONGODB_USERNAME}:${MONGODB_PASSWORD}@${MONGODB_HOSTNAME}:${MONGODB_PORT}`,
    MONGODB_CONNSTRING_WITH_DB: `mongodb://${MONGODB_USERNAME}:${MONGODB_PASSWORD}@${MONGODB_HOSTNAME}:${MONGODB_PORT}/${dbName}?authSource=admin`
  };
}
