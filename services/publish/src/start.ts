import { MongoClient } from 'mongodb';

import createApp from './app';
import { getMongoClient, setupMongoDB } from './db';
import startWorker from './startWorker';
import runPreflightChecks from './utils/preflight_checks';

/**
 * Main entry point
 */
async function main(): Promise<void> {
  await runPreflightChecks();

  console.log('Preflight is OK, starting server');

  await setupMongoDB();
  await startWorker();

  await createApp({
    // logger: createLogger({
    // name: 'api'
    // transport: {
    //   target: 'pino-pretty',
    //   options: {
    //     colorize: true
    //   }
    // }
    // })
  });
  process.on('exit', async () => {
    console.log('About to exit, waiting for remaining connections to complete');
    const db: MongoClient = await getMongoClient();
    db.close();
  });
}

main().catch(console.error);
