import Agenda, { Job } from 'agenda';

import { dbName, setupMongoDB } from './db';
import configureBuildOperation from './jobs/publishOperation';
import configureBuildWorkflow from './jobs/publishWorkflow';
import { verifyEnv } from './utils/env';

verifyEnv([]);

export const jobsCollectionName: string = 'agendaJobs';

export let cachedWorker: Agenda;

function time(): string {
  return new Date().toTimeString().split(' ')[0];
}

export async function createWorker(): Promise<Agenda> {
  const mc = (await setupMongoDB()).db(dbName);
  const agenda: Agenda = new Agenda({
    mongo: mc
  });

  await mc.collection(jobsCollectionName).createIndex(
    { 'data.index': 1 },
    {
      unique: true,
      name: 'index'
    }
  );

  configureBuildOperation(agenda);
  configureBuildWorkflow(agenda);
  cachedWorker = agenda;

  return agenda;
}

export async function startWorker(): Promise<void> {
  console.log('Starting the worker instance');

  const agenda = await createWorker();
  await agenda.start();
  // @TODO not implemented
  // await jobQueue.every('3 minutes', 'delete finished jobs');
  // Log job start and completion/failure
  agenda.on('start', (job) => {
    console.log(time(), `Job <${job.attrs.name}> starting`);
  });
  agenda.on('success', async (job: Job) => {
    console.log(time(), `Job <${job.attrs.name}> succeeded`);
    // await job.remove();
  });
  agenda.on('fail', (error, job) => {
    console.log(time(), `Job <${job.attrs.name}> failed:`, error);
  });
}

// // stop the job-scheduler gracefully
// async function graceful(): Promise<void> {
//   console.log(cachedWorker);

//   // await cachedWorker.stop();
//   process.exit(0);
// }

// process.on('SIGTERM', graceful);
// process.on('SIGINT', graceful);
export default startWorker;
