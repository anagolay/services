# Publish service

The easiest way to start this service is using OCI image we are hosting here https://hub.docker.com/repository/docker/anagolay/microservices-publish. We suggest using the `latest` tags ALWAYS.

The service depends on the MongoDB to be up and running before. We follow default per dependent image, like the `27017` for the MongoDB port.

Additionally the service needs IPFS and we are keeping it as defaulted to `5001` port and `127.0.0.1`. You can change that in the env file too.

## Local development:

Almost all setup is done when you launch the devcontainer. Next steps needs to be done to get the doppler working.

1. create the `access` token for the `dev_devcontainer` env in the `publish` project
2. run `doppler configure set token REPLACE_MEEEEEE --scope=/workspace/services/publish`
3. Now, to enable the env run `cd /workspace/services/publish`. It might appear error which says you need to run the `direnv allow`, do it! This will load the all the needed env in your shell
4. `pnpm start`
