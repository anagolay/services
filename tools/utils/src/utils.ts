import { MimeType } from 'file-type';
import * as mime from 'mime-types';
import pino, { Logger as PinoLogger, LoggerOptions } from 'pino';

/**
 * checks against the common value options for the true boolean value
 * @param value - Can be `'true' | '1' | 1`
 * @returns Boolean
 * @public
 */
export function isTrue(value: string | number): boolean {
  return value === 'true' || value === '1' || value === 1 ? true : false;
}

/**
 * checks against the common value options for the false boolean value
 * @param value - Can be `'false' | '0' | 0`
 * @returns Boolean
 * @public
 */
export function isFalse(value: string | number): boolean {
  return value === 'false' || value === '0' || value === 0 ? true : false;
}

/**
 * elapsed time
 * @param start - a 'process.hrtime()' instance
 * @returns Elapsed time in milliseconds
 * @public
 */
export function elapsed_time(start: [number, number], round: boolean = false): number {
  const elapsed = process.hrtime(start)[1] / 1000000; // divide by a million to get nano to milliseconds

  if (round) {
    return Math.floor(elapsed);
  }

  return elapsed;
}

/**
 * Re-Export Pino logger
 * @public
 */
export type Logger = PinoLogger;
/**
 * Create the PINO logger with pino pretty if in DEV
 * @param options -
 * @returns
 * @public
 */
export function createLogger(options?: LoggerOptions): Logger {
  const logger = pino({
    ...options
  });
  return logger;
}

/**
 * Get correct mime type for the browser and the file
 * @param extension - A file extension, like png or jpeg
 * @public
 */
export function getContentTypeFromExtension(extension: string): MimeType {
  const resolvedType = mime.lookup(extension);
  if (!resolvedType) {
    throw new Error(`Cannot infer the content type for ${extension}`);
  } else {
    return resolvedType as MimeType;
  }
}
