import { isFalse, isTrue } from '.';

describe('Utils suite', () => {
  describe('isTrue suite', () => {
    it('should return correct value', () => {
      expect(isTrue(1)).toBe(true);
      expect(isTrue('1')).toBe(true);
      expect(isTrue('true')).toBe(true);
    });
    it('should fail correct value', () => {
      expect(isTrue(2)).toBe(false);
      expect(isTrue('2')).toBe(false);
      expect(isTrue('truedasda')).toBe(false);
    });
  });
  describe('isFalse suite', () => {
    it('should return correct value', () => {
      expect(isFalse(0)).toBe(true);
      expect(isFalse('0')).toBe(true);
      expect(isFalse('false')).toBe(true);
    });
    it('should fail correct value', () => {
      expect(isFalse(2)).toBe(false);
      expect(isFalse('2')).toBe(false);
      expect(isFalse('truedasda')).toBe(false);
    });
  });
});
