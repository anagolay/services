import { exec as originalExec, ExecOptions as BaseExecOptions, PromiseWithChild } from 'child_process';
import { promisify } from 'util';

/**
 * Extended `exec`options
 * @public
 */
export interface IExecOptions extends BaseExecOptions {
  encoding?: BufferEncoding;
}

/**
 * Default exec options
 * @public
 */
export const defaultExecOptions: IExecOptions = {
  cwd: '.',
  encoding: 'utf8'
};

/**
 * Async Exec with decent defaults
 * @param command - A command you would put in the `exec`
 * @param options - Any of the {@link IExecOptions} options
 * @returns
 */
export default async function exec(
  command: string,
  options: IExecOptions = defaultExecOptions
): Promise<
  PromiseWithChild<{
    stdout: string;
    stderr: string;
  }>
> {
  const execAsync = promisify(originalExec);
  return execAsync(command, options);
}
