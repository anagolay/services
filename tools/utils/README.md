# Anagolay Utilities

Lots of useful Utility functions for Anagolay ecosystem

We encourage you to join us building Anagolay and its ecosystem!

Find us here:

- [Discord](https://discordapp.com/invite/WHe4EuY)
- [Twitter](https://twitter.com/AnagolayNet)
- [Matrix](https://matrix.to/#/#anagolay-general:matrix.org)
- [Website](https://anagolay.network/?utm_medium=pacakge&utm_source=npm)
